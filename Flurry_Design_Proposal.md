# Flurry Analytics Design Proposal 1.0



### ID: View_Shown
----
##### Name:
- Mustache View
- Nested List View
- Over View
- Notification View
- Drawer View
- Paging View
- Unlock View
- Nested List View

##### Title:
- (title if there is one)

##### Tipsheet:
- If applicable. The content is uncertain at this point, since there are often many clinical terms associated with one tipsheet (and, for that matter, listed as a group in the cell)

##### Index:
- (index if paging view)

##### Example 1:
	ID: View_Shown
	Name: Nested List View
	Title: Documentation Tips
	Tipsheet: Bacteremia

##### Example 2:
	ID: View_Shown
	Name: Paging View
	Index: 1
	?? Will our paging views have titles either explicitly in the app or behind the scenes for our purposes ??

### ID: Cell_Selected
----
##### Cell_Text:
- (cell text)

##### On_View_Name:
- (current view name)

##### On_View_Title:
- (current view title)

##### On_Drawer_Item OR On_Tab_Bar_Item:
- (current drawer or tab item selected)

##### On_Scope:
- (current scope; eg. "All Categories" or "Frequently Used")

##### Filter_Text:
- (text entered in search field if any)

##### Topic:
- (topic name if any)

##### Subcategory:
- (subcategory if any)

##### Category:
- (category if any)


##### EXAMPLE 1:
	ID: Cell_Selected
	Cell_Text: Hematology
	On_View_Name: Nested List View
	On_View_Title: Documentation Tips
	On_Drawer_Item: Documentation Tips
	On_Scope: All Categories
	Category: Hematology

##### EXAMPLE 2:
	ID: Cell_Selected
	Cell_Text: Hematology
	On_View_Name: Nested List View
	On_View_Title: Documentation Tips
	On_Drawer_Item: Documentation Tips
	On_Scope: All Categories
	Filter_Text: Hemat
	Category: Hematology

##### EXAMPLE 3:
	ID: Cell_Selected
	Cell_Text: DVT/PE
	On_View_Name: Nested List View
	On_View_Title: Documentation Tips
	On_Drawer_Item: Documentation Tips
	On_Scope: All Categories
	Filter_Text: Hemat
	Subcategory: DVT/PE
	Category: Hematology

##### EXAMPLE 4:
	ID: Cell_Selected
	Cell_Text: Blood Thinning
	On_View_Name: Nested List View
	On_View_Title: Documentation Tips
	On_Drawer_Item: Documentation Tips
	On_Scope: All Categories
	Filter_Text: Hemat
	Topic: Blood Thinning (*This is a hypothetical 3rd level of depth)
	Subcategory: DVT/PE
	Category: Hematology

### ID: Button_Pressed
----
##### Button_Text:
- (button text)

##### On_View_Name:
- (current view name)

##### On_View_Title:
- (current view title)

##### On_Drawer_Item OR On_Tab_Bar_Item:
- (current drawer or tab item selected)

##### EXAMPLE:
	ID: Button_Pressed
	Button_Text: Unlock
	On_View_Name: Unlock View
	On_View_Title: Documentation Tips
	On_Drawer_Item: Documentation Tips

### ID: Unlock_Attempted
---
##### Email:
- (email entered)

##### Pin:
- (pin entered)

##### Successful:
- (yes/no)


##### Example:
	ID: Unlock_Attempted
	Email: chris@selfcare.info
	Pin: 0000
	Successful: YES
	
### ID: Tab_Selected
----
##### Tab_Item_Text:
- (tab text)

##### On_Tab:
- (current tab)

##### Example:
	ID: Tab Selected
	Tab_Item_Text: Alphabetical Index
	On_Tab: Favorites

### ID: Drawer_Item_Selected
----
##### Drawer_Item_Text:
- (drawer item text)

##### On_Drawer_Item:
- (current drawer item)

##### Example:
	ID: Drawer_Item_Selected
	Drawer_Item_Text: CDI Program
	On_Drawer_Item: Documentation Tips

### ID: Text Submitted
----
##### Text(#):
- (text submitted; if multiple fields are submitted, then use numbers incrementing from 1)

##### Submit_Button_Text:
- (text in the button used to submit the text)

##### Example:
	ID: Text Submitted
	Text1: chris@selfcare.info
	Text2: 0000
	Submit_Button_Text: Unlock

### To Do:
----
- need to talk with joseph and erik about how we want to handle tracking dead end filter words because there are many ways we could do it, all with varying implementation techniques--thus, I feel those implementing it should have a prominent say in the matter.
- generate use cases



