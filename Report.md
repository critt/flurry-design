----
#Flurry Analytics: Summary of Findings
----
##Pros
----
- Some of the widgets are prettier and more interactive than Google Analytics widgets.
	- The map widget, for example, is interactive (you can click down and zero in on regions, starting at a world view and going all the way down to a metro view)
- Custom event parameters
	- Very enticing in that we always have felt constrained by Google's event parameter model
- Baked-in demographic analytics
	- An excellent, easy way to get interesting demographic information about your users

##Cons
----
- Limited querying and reporting
	- Only possible to filter out specific events in the Event Summary
	- No concept of unique reports
- Inherent inconsistency in data across different features in the portal
	- Data shows up in logs first, then in the Event Summary hours or days later. When the data becomes "real" is ambiguous
- Takes much longer for traffic to show up anywhere in the portal besides logs
	- Makes testing difficult
	- Inherently lower integrity of data due to inherent discrepancy between logs and "real" data
- Doesn't send session data  until the app is closed
	- This includes all calls representing event occurances
	- Makes using in a virtual machine tricky (at least a little less efficient)
	- Makes testing slightly more difficult
- Less powerful querying in widget design
	- Flurry only allows the use of one filter per widget, whereas Google Analytics allows multiple regex filters
- Not a viable product for an analytics solution
	- Can't share dashboards
	- Portal is more confusing and harder to use than the Google Analytics portal
	- People likely have never used it
- No cross platform aggregation of apps
	- Forces separation of iOS and Android apps in the portal

##Summary
----
For the most part, the only things Flurry has going for it are its custom event parameters and baked in demographic analysis features. Apart from this, the portal is drastically less powerful and drastically less flexible than Google's. Events themselves can be much more information-rich in Flurry, but unfortunately the Flurry portal fails in providing sufficiently useful tools in working with the data. While the demographic analytics would be nice, and while it would be nice to pack more information in events, we have come to require certain features of our analytics platform not just as a software company but also as a company that provide analytics solutions (like granting clients access while retaining ownership, being able to query for freely in reports and widgets, and having a certain level of confidence in the intergrity of the data), such that relying on Flurry as a primary analytics platform for anything seems out of the question.

I would recommend continuing to use Google Analytics as our primary analytics platform. If we want something like custom, arbitrary event parameters, perhaps we can accomplish that end through some other kind of implementation on the client side. And if we want to take advantage of the demographic analytics features of Flurry then perhaps we can have our apps talk to Flurry once in a while during their lifetime with some innocuous call, just so we can go in and see what Flurry thinks our user base is like demographic-wise. 

